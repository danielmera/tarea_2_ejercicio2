package pilas;


import java.io.*;

public class Pila {
 private static BufferedReader entrada = new BufferedReader(new InputStreamReader(System.in));
 private static int MAX_LENGTH = 5;
 private static String Pila[] = new String[MAX_LENGTH];
 private static int cima = -1;
   
    
public static void Menu()throws IOException{
     System.out.println("\n\n\t\t\t=========Menú Manejo Pila=============");
     System.out.println("\t\t\t=                                    =");
     System.out.println("\t\t\t= 1- Insertar elemento               =");
     System.out.println("\t\t\t= 2- Eliminar elemento               =");
     System.out.println("\t\t\t= 3- Buscar elemento                 =");
     System.out.println("\t\t\t= 4- Imprimir pila                   =");
     System.out.println("\t\t\t= 5- Salir                           =");
     System.out.println("\t\t\t======================================");
     System.out.print("\t\t\tOpcion: ");
     int op = Integer.parseInt(getEntrada().readLine());
     Opciones(op);
    }
    public static void Opciones(int op)throws IOException{
        switch(op){
			case 1: Insertar();
			        break;
			case 2: Eliminar();
			        break;
			case 3: Buscar();
			        break;
			case 4: Imprimir();
			        break;
			case 5: System.exit(0);
			        break;
			default:Menu();
			        break;
	   }
    }

    private static void Insertar()throws IOException{
       System.out.print("\nDigite algo para la pila: ");
       String dato = getEntrada().readLine();
       Crear(dato);
    }

    private static void Crear(String dato)throws IOException{
      if ((getPila().length-1)==getCima()){
        System.out.println("Capacidad de la pila al limite\n\n\n");
        Imprimir();
      } else{
            setCima(getCima() + 1); //incremento la variable de control cima
            Agregar(dato);
        }

    }

    public Pila() {
    }
    private static void Agregar(String dato)throws IOException{
        getPila()[getCima()]=dato; //guardo el dato en la posición donde esta cima
      Menu();
    }
    private static void Imprimir()throws IOException{
      for(int i=getPila().length-1;i>=0;i--){
	       System.out.println(getPila()[i]);
      } //violaciòn al TAD
      Menu();
    }
    private static void Eliminar()throws IOException{
      if(getCima()== -1){ //si pila esta vacia
		  System.out.println("\n\n\nNo se puede eliminar, pila vacía !!!" );
      } else{
              System.out.println("se eliminó" + getPila()[getCima()]);
	      getPila()[getCima()] = null;
           
	      setCima(getCima() - 1);
        }
      Menu();
    }
    private static void Buscar()throws IOException{
       System.out.println("\n\n\nDigite la cadena a buscar: ");
       String cad = getEntrada().readLine();
       for(int i=0;i<getPila().length-1;i++){
			if(cad.equals(getPila()[i])){
				System.out.println("Elemento encontrado,posición "+i);
				break;
		        } else{
		     	       System.out.println("Elemento no encontrado :(");
	                      }
        }
	Menu();
    }

   
    public static BufferedReader getEntrada() {
        return entrada;
    }

   
    public static void setEntrada(BufferedReader aEntrada) {
        entrada = aEntrada;
    }

    /**
     * @return the MAX_LENGTH
     */
    public static int getMAX_LENGTH() {
        return MAX_LENGTH;
    }

    /**
     * @param aMAX_LENGTH the MAX_LENGTH to set
     */
    public static void setMAX_LENGTH(int aMAX_LENGTH) {
        MAX_LENGTH = aMAX_LENGTH;
    }

    /**
     * @return the Pila
     */
    public static String[] getPila() {
        return Pila;
    }

    /**
     * @param aPila the Pila to set
     */
    public static void setPila(String[] aPila) {
        Pila = aPila;
    }

    /**
     * @return the cima
     */
    public static int getCima() {
        return cima;
    }

    /**
     * @param aCima the cima to set
     */
    public static void setCima(int aCima) {
        cima = aCima;
    }
    
}

